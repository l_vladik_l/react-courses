// Components
import GlobalStyle from './components/GlobalStyle';
import Modal from './components/Modal';
// styled
import styled from 'styled-components';
import {useState} from 'react';

function App() {
	const [show, setShow] = useState(false);
	const [showS, setShowS] = useState(false);

	const popHandler = () => {
		setShow(!show);
	};

	const popHandlers = () => {
		setShowS(!showS);
	};

	return (
		<div className="App">
			<GlobalStyle />

			<Wrapper>
				<ButtonClr onClick={popHandler}>
					<h3>Open first Modal</h3>
				</ButtonClr>
				<button onClick={popHandlers}>
					<h3>Open second Modal</h3>
				</button>
			</Wrapper>

			{show && (
				<Modal
					actions={
						<WrappBtnsChoose>
							<button>Ok</button>
							<button>Cancel</button>
						</WrappBtnsChoose>
					}
					head={'Do you want to delete this file?'}
					show={show}
					setShow={setShow}
					backColor="lightgrey"
					text="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, repudiandae"
				/>
			)}

			{showS && (
				<Modal
					actions={
						<WrappBtnsChoose>
							<button>never click this button!</button>
							<button>
								<>
									are you sure <br></br>you want to touch this?
								</>
							</button>
						</WrappBtnsChoose>
					}
					head={'Hello how are you'}
					show={showS}
					setShow={setShowS}
					backColor="blue"
					text="Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam sint, doloremque esse quam unde laudantium dolorem itaque id, rem accusamus natus, temporibus distinctio magnam provident sit. Perspiciatis voluptate quod aliquam eius, iusto ratione iste quasi magni unde officiis alias modi?"
				/>
			)}
		</div>
	);
}

const Wrapper = styled.header`
	width: 100%;
	height: 20vh;
	background-color: lightblue;
	display: flex;
	justify-content: space-evenly;
	align-items: center;
	padding: 20px 40px;
	button {
		border: none;
		padding: 1.5rem 4.2rem;
		border-radius: 1rem;
		box-shadow: 0px 5px 30px rgba(0, 0, 0, 0.5);
		outline: none;
		font-size: 1rem;
		width: 30%;
	}
`;

const WrappBtnsChoose = styled.div`
	padding: 0px 95px;
	width: 100%;
	display: flex;
	justify-content: space-around;
	align-items: center;

	button {
		padding: 10px 15px;
		border-radius: 5px;
		font-size: 15px;
		font-weight: bold;
		border: none;
		outline: none;
	}
`;

const ButtonClr = styled.button`
	background-color: #944646;
`;

export default App;
