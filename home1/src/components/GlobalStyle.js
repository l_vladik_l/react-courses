import {createGlobalStyle} from 'styled-components';

const GlobalStyle = createGlobalStyle`
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    body{
        font-family: "Montserrat", sans-serif;
        width: 100%;
    }
    a[href], input[type='submit'], input[type='image'], label[for], select, button, .pointer {
       cursor: pointer;
}
`;

export default GlobalStyle;
