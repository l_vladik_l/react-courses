import React from 'react';
import styled from 'styled-components';
import close from '../img/iconmonstr-x-mark-1-48.png';

const Modal = ({head, backColor, text, setShow, show, actions}) => {
	const exitCardHandler = (e) => {
		const element = e.target;
		if (element.classList.contains('shadow')) {
			setShow(!show);
		}
	};

	const closeDivHandler = () => {
		setShow(!show);
	};

	return (
		<CardShadow className="shadow" onClick={exitCardHandler}>
			<Card style={{backgroundColor: backColor}}>
				<AddWrapper>
					<CloseTeg onClick={closeDivHandler}>
						<img src={close} alt="close" />
					</CloseTeg>
					<LineDrawAfterDiv>
						<LineDrawAfter>{head}</LineDrawAfter>
					</LineDrawAfterDiv>
					<TextAlign>
						<h3>{text}</h3>
					</TextAlign>
					{actions}
				</AddWrapper>
			</Card>
		</CardShadow>
	);
};

const CardShadow = styled.div`
	width: 100%;
	min-height: 100vh;
	background-color: rgba(0, 0, 0, 0.9);
	position: fixed;
	top: 0;
	left: 0;
`;

const Card = styled.div`
	width: 45%;
	height: 50%;
	border-radius: 1rem;
	padding: 1rem 1.5rem;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	box-shadow: 0px 15px 1500px rgba(255, 255, 255, 0.2);
	/* background-color: rgba(218, 53, 94, 0.5); */
`;

const AddWrapper = styled.div`
	position: relative;
	height: 100%;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	padding: 20px 0px;
`;

const TextAlign = styled.div`
	text-align: center;
	line-height: 30px;
`;

const LineDrawAfterDiv = styled.div`
	position: relative;
	text-align: center;
	width: 100%;
	font-size: 18px;
	font-weight: bold;
`;
const LineDrawAfter = styled.div`
	&:after {
		content: '';
		position: absolute;
		bottom: -25px;
		left: 0;
		width: 100%;
		height: 3px;
		background-color: white;
	}
`;
const CloseTeg = styled.div`
	position: absolute;
	top: 0%;
	right: 0%;
	width: 25px;
	height: 25px;

	img {
		width: 100%;
		object-fit: cover;
		cursor: pointer;
		z-index: 100;
	}
`;

export default Modal;
